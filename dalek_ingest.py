#!/usr/bin/env python
""" Elasticsearch 5 classes and tools for pipeline creation, ingest and post-ingest data manipulation """
import os
import json
import argparse
import logging
import glob
import yaml
from dalek import Pattern, Pipeline, Index
from dalek.utils import FileInfo, Config, define_pattern, load_plugins
from dalek.constants import DEFAULT_BULK_SIZE


# Pattern and index defaults
DEFAULT_PATTERN_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'patterns')
DEFAULT_PROCESSORS_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'processors')
DEFAULT_PATTERN_KEYS = '["^MOVIE_.*"]'
DEFAULT_PATTERN_INCLUDE = os.path.join(DEFAULT_PATTERN_PATH, '_include_')


def _get_args_dict(args, parser):
    """ Convert parser to nested dictionary """
    config = Config()
    args_dict = dict()
    for parser_ in parser._action_groups:
        p_keys = [action.dest for action in parser_._group_actions]
        p_names = {name: value for (name, value) in args._get_kwargs() if name in p_keys}
        args_dict[parser_.title] = p_names
    config.update(args_dict)
    return config


def get_config(args, parser):
    config = _get_args_dict(args, parser)
    if not isinstance(config.files, list):
        config.files = [config.files]

    # Merge config file values with command line arguments
    if args.config:
        # load te configuration file
        with open(args.config) as fd:
            config.update(yaml.load(fd))

    # JSON load pattern keys and definition keys
    if config.pattern.pattern_keys:
        try:
            config.pattern.pattern_keys = json.loads(config.pattern.pattern_keys)
        except Exception as ex:
            raise Exception("Failed to load JSON from pattern keys: %s. Error: %s" % (config.pattern.pattern_keys, ex))

    # Read extra data for pipeline upload
    if config.general.extra_data:
        if os.path.isfile(config.general.extra_data):
            with open(config.general.extra_data) as fd:
                config.general.extra_data = json.loads(fd.read())
        else:
            config.general.extra_data = json.loads(config.general.extra_data)

    # If the pattern path is a directory, a log file is required to auto-detect the pattern
    if os.path.isdir(config.pattern.pattern_path):
        if not config.files:
            raise Exception("An input file is required if the pattern path is directory")

    if config.general.plugin_path:
        config.plugins = load_plugins(config.general.plugin_path)

    return config


def main():
    """ Main function """
    parser = argparse.ArgumentParser(description='Convert logstash pattern to elasticsearch pipeline')
    g_parser = parser.add_argument_group('general')
    g_parser.add_argument('--debug', action='store_true', help='debug logging')
    g_parser.add_argument('--config', help='Configuration file')

    p_parser = parser.add_argument_group('pattern')
    p_parser.add_argument('--pattern-path', default=DEFAULT_PATTERN_PATH, type=str, help='pattern path')
    p_parser.add_argument('--pattern-keys', default=DEFAULT_PATTERN_KEYS, type=str, help='pattern key list')
    p_parser.add_argument('--pattern-include-file', default=DEFAULT_PATTERN_INCLUDE, type=str, help='pattern include file')
    p_parser.add_argument('--pattern-multiline-method', default=None, type=str, help='default multiline method')

    pl_parser = parser.add_argument_group('pipeline')
    pl_parser.add_argument('--host', type=str, help='hostname of elasticsearch api')
    pl_parser.add_argument('--create', action="store_true", help='create pipeline on elasticsearch')
    pl_parser.add_argument('--overwrite', action="store_true", help='overwrite existing pipeline on elasticsearch')
    pl_parser.add_argument('--upload', action='store_true', help='upload file after pipeline validation')
    pl_parser.add_argument('--simulate', action='store_true', help='simulate file upload after pipeline validation')
    pl_parser.add_argument('--ignore-errors', action="store_true", help='Ignore errors during simulate and upload')
    pl_parser.add_argument('--bulk-size', type=int, default=DEFAULT_BULK_SIZE, help='Upload Bulk size for ElasticSearch API')

    i_parser = parser.add_argument_group('index')
    i_parser.add_argument('--index-name', type=str, help='index name')

    f_parser = parser.add_argument_group('files')
    f_parser.add_argument('--extra-data', help='Additional data for each uploaded message')
    f_parser.add_argument('--filepath', type=str, help='file path of input file')

    # Get configuration
    args = parser.parse_args()
    config = get_config(args, parser)

    # Setup logging
    loglevel = logging.DEBUG if config.general.debug else logging.INFO
    fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=loglevel, format=fmt)

    for file_info in config.files:
        file_obj = FileInfo()
        file_obj.update(file_info)
        file_obj.multiline.method = file_obj.multiline.method or config.pattern.pattern_multiline_method
        ignore_errors = file_obj.ignore_errors or config.pipeline.ignore_errors

        for filepath in glob.glob(file_obj.filepath):
            # Preprocess file
            if file_obj.preprocessor and config.plugins:
                preprocess = config.plugins.get(file_obj.preprocessor)
                filepath, file_obj = preprocess(filepath, file_obj)

            # Parse patterns from file
            if os.path.isdir(config.pattern.pattern_path):
                pipeline_name, pattern_filepath = define_pattern(filepath, config.pattern.pattern_path)
            else:
                pattern_filepath = config.pattern.pattern_path
                pipeline_name = os.path.basename(config.pattern.pattern_path)

            pattern = Pattern(pattern_filepath, config.pattern.pattern_include_file, config.pattern.pattern_keys)
            pattern.load()

            # Create pipeline from pattern
            pipeline = Pipeline(pipeline_name, bulk_size=config.pipeline.bulk_size)
            pipeline.prepare(pattern)

            if filepath and (config.pipeline.upload or config.pipeline.simulate):
                messages = pipeline.get_messages(filepath, file_obj)
            else:
                messages = list()

            if config.pipeline.host and config.pipeline.simulate and not config.pipeline.upload:
                pipeline.simulate(config.pipeline.host, messages, ignore_errors)
            if config.pipeline.create or (config.pipeline.upload and config.pipeline.overwrite):
                pipeline.create(config.pipeline.host, config.pipeline.overwrite)
            if config.pipeline.upload:
                index_name = config.index.index_name or pipeline_name
                index = Index(index_name, config.pipeline.bulk_size)
                index.upload(config.pipeline.host, messages, pipeline.name, config.general.extra_data,
                             config.pipeline.overwrite, ignore_errors)


if __name__ == '__main__':
    main()
