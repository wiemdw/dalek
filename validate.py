#!/usr/bin/env python
""" Grok pattern validator """
import os
import re
import json
import logging
import argparse
import collections
from pygrok import Grok


logging.basicConfig(level=logging.INFO, format='%(message)s')

DEFAULT_PATTERN_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'patterns')
DEFAULT_PATTERN_KEYS = '[".*_MESSAGE$"]'
DEFAULT_PATTERN_INCLUDE = os.path.join(DEFAULT_PATTERN_PATH, '_include_')


# TODO - find duplicate pattern names

def read_pattern_file(filepath, pattern_keys=None):
    """Read grok pattern file"""
    patterns = collections.OrderedDict()
    definitions = dict()
    comments = list()

    with open(filepath) as fd:
        for line in fd:
            line = line.strip()
            if not line:
                continue
            if line.startswith('##'):
                comments.append(''.join(line[2:]))
            elif pattern_keys:
                parts = line.split()
                for pattern_key in json.loads(pattern_keys):
                    if re.findall(pattern_key, parts[0]):
                        patterns[parts[0]] = {'pattern': ' '.join(parts[1:]),
                                              'text': comments}
                        comments = list()
                    else:
                        definitions[parts[0]] = ' '.join(parts[1:])
            else:
                parts = line.split()
                definitions[parts[0]] = ' '.join(parts[1:])

    logging.debug("Pattern names for %s: %s", filepath, patterns.keys())
    return patterns, definitions


def validate_pattern(pattern_file, pattern_keys, include_file=None, pattern=None):
    """Validate patterns

    :param pattern_file: pattern file path
    :param pattern: pattern name to validate (default: all)

    """
    logging.info('%s (%s)', os.path.basename(pattern_file).upper(), pattern_file)

    patterns, definitions = read_pattern_file(pattern_file, pattern_keys)

    if include_file:
        _, include = read_pattern_file(include_file)
        definitions.update(include)

    for key, value in patterns.iteritems():
        if pattern and not re.match(pattern, key):
            continue  # exclude by pattern argument

        logging.info("==> Validating %s (%s) with %s examples", key, value.get('pattern'), len(value['text']))
        if not 'text' in value:
            continue  # no pattern comments
        grok = Grok(value['pattern'], custom_patterns=definitions)
        for line in value['text']:
            match = grok.match(line)
            if not match:
                logging.error("%s does not match:\n%s", key, line)
            else:
                logging.debug("input: %s", line)
                logging.debug("matched: %s", json.dumps(match, indent=2))


def dump_pattern_vars(pattern_file, pattern_keys=None, include_file=None, pattern=None):
    """Show patterns and variables used

    :param pattern_file: pattern file path
    :param pattern: pattern name to validate (default: all)

    """
    logging.info('%s (%s)', os.path.basename(pattern_file).upper(), pattern_file)

    patterns, definitions = read_pattern_file(pattern_file, pattern_keys)

    if include_file:
        _, include = read_pattern_file(include_file)
        definitions.update(include)

    regex_vars = list()
    for key, value in patterns.iteritems():
        if pattern and not re.match(pattern, key):
            continue  # exclude by pattern argument

        grok = Grok(value['pattern'], custom_patterns=definitions)
        regex_pattern = grok.regex_obj.pattern
        regex_vars.extend(re.findall(r'\<([a-z_]*?)\>', regex_pattern))

    logging.info("==> %s", ','.join(sorted(set(regex_vars))))


def main():
    parser = argparse.ArgumentParser(description='Convert logstash pattern to elasticsearch pipeline')
    parser.add_argument('--debug', action='store_true', help='debug logging')
    # Pattern arguments
    parser.add_argument('--path', default=DEFAULT_PATTERN_PATH, type=str, help='pattern path')
    parser.add_argument('--keys', default=DEFAULT_PATTERN_KEYS, type=str, help='pattern key list')
    parser.add_argument('--include', default=DEFAULT_PATTERN_INCLUDE, type=str, help='pattern include file')
    # Filters
    parser.add_argument('--file', help='Pattern file path')
    parser.add_argument('--pattern', help='Pattern name')
    # Actions
    parser.add_argument('--dump-vars', action='store_true', help='Dump pattern variables')
    parser.add_argument('--validate', action='store_true', help='Validate pattern file(s)')

    args = parser.parse_args()

    if args.debug:
        logging.root.setLevel(logging.DEBUG)

    # Set correct action
    action = dump_pattern_vars if args.dump_vars else validate_pattern

    if args.file:
        try:
            action(args.file, args.keys, args.include, args.pattern)
        except Exception as ex:
            logging.exception("Exception caught for %s", args.file)
    else:
        for filename in os.listdir(args.path):
            try:
                action(os.path.join(args.path, filename), args.keys, args.include, args.pattern)
            except Exception as ex:
                logging.exception("Exception caught for %s", filename)


if __name__ == '__main__':
    main()
