""" Elasticsearch Index Class """
import sys
import json
import time
import logging
import requests
from elasticsearch import Elasticsearch, NotFoundError, helpers
from dalek.constants import DEFAULT_BULK_SIZE, BULK_URL, SEARCH_URL, SCROLL_URL


class Index(object):
    """ Elasticsearch index class."""

    def __init__(self, name, bulk_size=None):
        """ Initialize index

        :param name: index name
        :param bulk_size: Elasticsearch bulk API size, used for uploading

        """
        self.name = name
        self.bulk_size = bulk_size or DEFAULT_BULK_SIZE

    def query(self, host, query_string, sort=None):
        """ Query index

        :param query_string: Query string (Elasticsearch Query DSL format)
        :param sort: Sort parameter

        :returns: generator
        """
        url = SEARCH_URL % (host, self.name)
        search = {
            'query': json.loads(query_string),
            'size': DEFAULT_BULK_SIZE,
        }
        if sort:
            search['sort'] = sort

        # Start scroll
        messages = json.loads(requests.get(url, data=json.dumps(search)).text)
        for msg in messages.get('hits', {}).get('hits', []):
            yield msg

        if '_scroll_id' in messages:
            scroll_id = messages['_scroll_id']
            search = {'scroll': '1m', 'scroll_id': scroll_id}
            url = SCROLL_URL % host

            # Continue scroll
            while scroll_id:
                messages = json.loads(requests.get(url, data=json.dumps(search)).text)
                scroll_hits = messages.get('hits', {}).get('hits', [])
                if scroll_hits:
                    for hit in scroll_hits:
                        yield hit
                else:
                    scroll_id = None

    def upload(self, host, messages, pipeline_name, extra_data=None, overwrite=False, ignore_errors=False):
        """Upload messages to Elasticsearch.

        :param host: Elasticsearch host (including port)
        :param pipeline_name: Name of the pipeline
        :param extra_data: Extra data to add to each document before uploading (dictionary)
        :param overwrite: Overwrite index if it already exists
        :param ignore_errors: Continue uploading after errors are returned from a batch

        """
        es = Elasticsearch(host)
        if not es.ping():
            raise Exception("elasticsearch host '%s' not reachable" % host)

        # Check if index already exists
        try:
            es.indices.get(self.name)
            if not overwrite:
                raise Exception("Index '%s' already exists" % self.name)
        except NotFoundError:
            pass  # does not exist

        actions = list()
        for msg in messages:
            msg_data = {
                "_op_type": "index",
                "_index": self.name,
                "_type": pipeline_name,
                "message": msg.strip()
            }

            if extra_data:
                msg_data.update(extra_data)

            actions.append(json.dumps(msg_data))
            if len(actions) == self.bulk_size:
                try:
                    bulk_r = helpers.bulk(es, actions, index=self.name, pipeline=pipeline_name, doc_type=pipeline_name)
                    logging.debug("Bulk upload response: %s", bulk_r)
                    sys.stdout.write('.')
                except Exception as ex:
                    if ignore_errors:
                        logging.warning("Bulk upload error: %s", ex)
                    else:
                        raise

                actions = list()

        # Process last batch of messages
        try:
            bulk_r = helpers.bulk(es, actions, index=self.name, pipeline=pipeline_name, doc_type=pipeline_name)
            logging.debug("Bulk upload response: %s", bulk_r)
        except Exception as ex:
            if ignore_errors:
                logging.warning("Bulk upload error: %s", ex)
            else:
                raise

        sys.stdout.write('\n')

    def add_field(self, host, messages, field_name, field_callback):
        """ Add a document field to each document in the index.

        :param host: Elasticsearch host (including port)
        :param messages: List or iterator to retrieve the documents
        :param field_name: Name of the new field to add
        :param field_callback: Callback function to set the field

        Example usage:
          index = Index(index_name)
          query_string = json.dumps({'term': {'_type': 'monitor'}})
          messages = index.query(host, query_string)
          index.add_field(host, messages, 'service_health', add_service_health)

        """
        actions = ''
        bulk_url = BULK_URL % host
        pre = time.time()

        for msg in messages:
            if field_name in msg['_source']:
                continue  # don't overwrite existing values

            field_value = field_callback(msg)
            if field_value is None:
                continue

            # update document
            action = json.dumps({"update": {"_index": self.name, "_type": msg['_type'], "_id": msg['_id']}})
            update = json.dumps({"doc": {field_name: field_value}})

            # update actions
            actions += '\n'.join([action, update]) + '\n'
            if len(actions) == self.bulk_size:
                bulk_r = requests.post(bulk_url, data=actions).text
                logging.debug("Bulk upload response: %s", bulk_r)
                actions = ''

        # Process last batch of messages
        bulk_r = requests.post(bulk_url, data=actions).text
        logging.debug("Bulk upload response: %s", bulk_r)

        logging.info("updating of messages completed: took %i seconds", time.time() - pre)

    def add_fields(self, host, messages, field_callback):
        """ Add a document field to each document in the index.

        :param host: Elasticsearch host (including port)
        :param messages: List or iterator to retrieve the documents
        :param field_callback: Callback function to set the field
        """
        for msg in messages:
            add_fields = field_callback(msg)
            if not add_fields:
                continue

            for field_name, field_value in add_fields.iteritems():
                # TODO - verify and test
                self.add_field(host, [msg], field_name, lambda fv: field_value)


#    def aggregate_field(self, host, query, field_name):
#        """ Aggregate field """
#        actions = ''
#        bulk_url = BULK_URL % host
#        pre = time.time()
#
#        field_value = None
#        pfv = False
#        for msg in self.query(host, query):
#            if field_name in msg['_source'] and msg['_source'][field_name]:
#                field_value = msg['_source'][field_name]
#            elif field_value:
#                # update document
#                action = json.dumps({"update": {"_index": self.name, "_type": msg['_type'], "_id": msg['_id'], '_routing': msg['_routing']}})
#                update = json.dumps({"doc": {field_name: field_value}})
#
#                # update actions
#                actions += '\n'.join([action, update]) + '\n'
#                if len(actions) == self.bulk_size:
#                    bulk_r = requests.post(bulk_url, data=actions).text
#                    logging.debug("Bulk upload response: %s", bulk_r)
#                    actions = ''
#
#        # Process last batch of messages
#        bulk_r = requests.post(bulk_url, data=actions).text
#        logging.debug("Bulk upload response: %s", bulk_r)
#
#        duration = time.time() - pre
#        logging.info("aggregation of field %s completed: %i messages took %i seconds", field_name, duration)
