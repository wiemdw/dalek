""" Elasticsearch Pipeline Class """
import os
import json
import logging
import requests
import pygrok
from dalek.messages import Messages
from dalek.constants import DEFAULT_BULK_SIZE


class Pipeline(object):
    """ Elasticsearch Pipeline class """
    def __init__(self, name, bulk_size=DEFAULT_BULK_SIZE, processors_path=None):
        """ Initialize pipeline

        :param name: Name of the pipeline
        :param bulk_size: Bulk API size for uploading
        :param processors_path: Path to additional pipeline processors

        """
        self.name = name
        self.bulk_size = bulk_size
        self.processors_path = processors_path
        self.pipeline = {
            "pipeline": {
                "description": "",
                "processors": []
            }
        }
        # Set by prepare() from Pattern object
        self.patterns = None
        self.pattern_definitions = None

    def prepare(self, pattern_obj):
        """Prepare pipeline from pattern object.

        :param pattern_obj: Pattern object

        """
        logging.info("creating pipeline")
        self.patterns, self.pattern_definitions = pattern_obj.convert_to_pipeline()

        current_list = self.pipeline['pipeline']['processors']
        for idx, pattern in enumerate(self.patterns):
            current_list.append({
                "grok": {
                    "field": "message",
                    "patterns": [pattern],
                    "pattern_definitions": self.pattern_definitions,
                    "on_failure": [],
                }
            })
            if idx+1 == len(self.patterns):
                # Empty 'on_failure' list on last grok processor is not allowed
                current_list[0]['grok'].pop('on_failure')
            else:
                current_list = current_list[0]['grok']['on_failure']

        # add additional processors from file
        processors_path = self.processors_path or os.path.join(os.path.dirname(pattern_obj.pattern_file), '..', 'processors')
        extra_processors = self.get_processors(processors_path)
        if extra_processors:
            if not isinstance(extra_processors, list):
                extra_processors = [extra_processors]
            self.pipeline['pipeline']['processors'].extend(extra_processors)

        logging.debug("pipeline: %s", json.dumps(self.pipeline, indent=4))
        return self.pipeline

    def get_processors(self, path):
        """Get additional processors from file."""
        processor_file = os.path.join(path, self.name)
        if os.path.isfile(processor_file):
            with open(processor_file) as fd:
                return json.loads(fd.read())
        return None

    def get_messages(self, filepath, file_info):
        """ Load messages from filepath. FileInfo object is used for encoding, start line,
        and multiline settings """
        logging.info("loading messages from %s", filepath)

        grok = None
        if self.patterns:
            if len(self.patterns) == 1 and self.patterns[-1].startswith('(?m)') and file_info.multiline.method is None:
                logging.info("auto-enabling multiline loading for %s", filepath)
                file_info.multiline.method = 'merge'

            # Only use the last pattern line (mainly for performance reasons). This is usually
            # a more generic catch-all pattern
            pattern = self.patterns[-1]
            # Setup PyGrok object
            grok = pygrok.Grok(pattern, custom_patterns=self.pattern_definitions)

        msg_reader = Messages(filepath, file_info, grok=grok)
        return msg_reader.load()

    def simulate(self, host, messages, ignore_errors=False):
        """ Simulate pipeline on an Elasticsearch host """
        url = 'http://%s/_ingest/pipeline/_simulate' % host

        logging.info("simulating pipeline to %s", url)
        logging.debug("Pipeline: %s", json.dumps(self.pipeline, indent=4))

        self.pipeline['docs'] = []
        # TODO - errors when more than 200.000 docs are added
        for message in messages:
            self.pipeline['docs'].append(
                {
                    "_source": {
                        "message": message
                    }
                }
            )

        req = requests.post(url, data=json.dumps(self.pipeline))
        req.raise_for_status()

        # validate every doc is correctly parsed
        response = json.loads(req.text)
        errors = list()
        for doc in response['docs']:
            if 'error' in doc:
                errors.append(doc)
            else:
                logging.debug('%s', json.dumps(doc, indent=4))
                # dump the original message and the values extracted from it
                for key, value in doc['doc']['_source'].iteritems():
                    logging.debug("  - %s: %s", key, value)

        if errors and ignore_errors is False:
            logging.error("parse errors: %s", json.dumps(errors))
            raise Exception("not all messages were correctly parsed (%s messages, %s errors)" % (len(messages), len(errors)))

        logging.info("simulation of pipeline with %i messages was successful", len(messages))
        logging.debug("simulation response: %s", json.dumps(response, indent=4))
        return response

    def create(self, host, overwrite=True):
        """Create the pipeline through Elasticsearch API."""
        url = 'http://%s/_ingest/pipeline/' % host

        logging.info("uploading pipeline %s to %s", self.name, url)

        existing = requests.get(os.path.join(url, self.name))
        if not overwrite and existing.status_code != 404:
            raise Exception("pipeline %s already exists. use '--overwrite' to overwrite" % self.name)

        response = requests.put(os.path.join(url, self.name), data=json.dumps(self.pipeline['pipeline']))
        if response.status_code != 200:
            raise Exception("failed to PUT pipeline %s: %s", self.name, response.text)

        logging.info("uploading pipeline %s was successful", self.name)
        logging.debug("put response: %s", json.dumps(response.text, indent=4))
