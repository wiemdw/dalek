""" Document loader """
import re
import gzip
import codecs
import logging
import collections
from functools import partial
from dalek.utils import is_gzipped, gunzip


class Messages(object):
    def __init__(self, filepath, file_info, grok=None):
        """ Initialize messages loader """
        self.filepath = filepath
        self.file_info = file_info
        self.grok = grok
        self.fd = self.open(filepath)
        self.skip()

    def open(self, filepath):
        """ Open the filepath, depending on compression and encoding """
        # Set the correct open() function, based on whether the file is gzipped or encoded
        if is_gzipped(filepath):
            if not self.file_info.encoding:
                file_open = gzip.open
            else:
                # Note: gzip compression does not support 'encoding' parameter in Python 2.x
                filepath = gunzip(filepath)
        if self.file_info.encoding:
            file_open = partial(codecs.open, encoding=self.file_info.encoding)
        else:
            file_open = open

        return file_open(filepath, mode='rb')

    def skip(self):
        """ Skip file header """
        if self.file_info.start_line:
            for _ in xrange(self.file_info.start_line):
                self.fd.next()

    def load(self):
        if self.file_info.multiline.method == 'merge':
            logging.info("loading %s with merge method", self.filepath)
            generator = self._load_merged()
        elif self.file_info.multiline.method == 'multiline':
            logging.info("loading %s with multiline method", self.filepath)
            generator = self._load_multiline()
        else:
            logging.info("loading %s with simple method", self.filepath)
            generator = self._load_lines()

        for item in generator:
            if not item or not item.strip():
                continue  # do not return empty messages
            yield item

    def _load_lines(self):
        """ Load lines from file """
        for line in self.fd:
            yield line

    def _load_multiline(self):
        """ Load multiline from file """
        re_line_delimiter = re.compile(self.file_info.multiline.pattern)
        include_delimiter = self.file_info.multiline.include_delimiter

        current_event = collections.deque([])
        for line in self.fd:
            if re_line_delimiter.match(line):
                if current_event:
                    yield '\n'.join(current_event)
                current_event.clear()
                if include_delimiter:
                    current_event.append(line)
            else:
                # add to line buffer
                current_event.append(line)

        # add remaining lines as last event
        if current_event:
            yield ''.join(current_event)

    def _load_merged(self):
        """ Load merged lines from file """
        current_event = None
        for line in self.fd:
            if line.startswith(' ') or line.startswith('}') or line.startswith(']') or (self.grok and not self.grok.match(line)):
                if not current_event:
                    continue
                # Message look like a multiline message: add it to the previous line(s)
                current_event += line
            else:
                yield current_event
                # Message can be pattern matched. Create a new messages list
                current_event = line

        # Returns the last message
        if current_event:
            yield current_event
