# Pattern and index defaults
DEFAULT_BULK_SIZE = 500
DEFAULT_SORT = [{"timestamp": {"order": "asc"}}]
DEFAULT_QUERY = {"query": {"match_all": {}}}

# URL templates
BULK_URL = 'http://%s/_bulk'
COUNT_URL = 'http://%s/%s/_count'
SEARCH_URL = 'http://%s/%s/_search?scroll=1m'
SCROLL_URL = 'http://%s/_search/scroll'

