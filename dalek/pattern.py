""" Elasticsearch Pattern Class """
import os
import re
import json
import logging
import collections
from copy import deepcopy
import pygrok


class Pattern(object):
    """Grok pattern class"""

    def __init__(self, pattern_file, include_file=None, pattern_keys=None):
        """Initialize.

        :param pattern_file: pattern file path
        :param include_file: file path for additional patterns to include
        :param pattern_keys: JSON list of pattern keys regexes to match

        """
        # file paths
        if not os.path.isfile(pattern_file):
            raise OSError("%s is not a pattern file" % pattern_file)
        self.pattern_file = pattern_file

        if include_file and not os.path.isfile(include_file):
            raise OSError("%s is not a file" % include_file)
        self.include_file = include_file

        self.name = os.path.basename(pattern_file)

        # list of pattern keys to use for conversion to pipeline
        if not pattern_keys:
            pattern_keys = list()
        elif isinstance(pattern_keys, list):
            self.pattern_keys = pattern_keys
        else:
            self.pattern_keys = json.loads(pattern_keys) or list()

        # dictionary of loaded patterns
        self.patterns = collections.OrderedDict()

        # dictionary of patterns and comments for validation
        self.patterns_custom = dict()
        self.definitions = None

    def load(self):
        """Load the pattern file and an optional include file. """
        logging.info("parsing %s and including %s", self.pattern_file, self.include_file)

        self.patterns = self._load(self.pattern_file)
        self.patterns.update(self._load(self.include_file))

        logging.info("found %s patterns in file %s", len(self.patterns), self.pattern_file)

        return self.patterns

    def _load(self, pattern_file):
        """Read grok pattern file into dictionary.

        :param pattern_file: pattern file path

        :returns: dict
        """
        logging.debug("parsing patterns from %s", pattern_file)

        # use OrderedDict to preserve to order of pattern keys in the file
        patterns = collections.OrderedDict()
        with open(pattern_file) as fd:
            for line in fd:
                line = line.strip()
                if not line or line.startswith('#'):
                    continue  # filter commented and empty lines

                grok_parts = line.strip().split()
                if not grok_parts or grok_parts[0] == '#':
                    continue
                # validate first part is the name of the pattern
                if not re.match(r'^[a-zA-Z0-9_]*$', grok_parts[0]):
                    raise Exception("invalid pattern name: %s" % grok_parts[0])

                patterns[grok_parts[0]] = ' '.join(grok_parts[1:])

        logging.debug("loaded %i patterns from %s", len(patterns), pattern_file)
        return patterns

    def _load_custom(self, filepath=None):
        """Parse grok pattern file with custom validation comments."""
        filepath = filepath or self.pattern_file
        patterns = collections.OrderedDict()
        definitions = dict()
        comments = list()

        with open(filepath) as fd:
            for line in fd:
                line = line.strip()
                if not line:
                    continue
                if line.startswith('##'):
                    comments.append(''.join(line[2:]))
                elif self.pattern_keys:
                    parts = line.split()
                    for pattern_key in self.pattern_keys:
                        if re.findall(pattern_key, parts[0]):
                            patterns[parts[0]] = {'pattern': ' '.join(parts[1:]),
                                                  'text': comments}
                            comments = list()
                        else:
                            definitions[parts[0]] = ' '.join(parts[1:])
                else:
                    parts = line.split()
                    definitions[parts[0]] = ' '.join(parts[1:])

        logging.debug("Pattern names for %s: %s", filepath, patterns.keys())

        if self.include_file and filepath != self.include_file:
            _, include = self._load_custom(self.include_file)
            definitions.update(include)
        else:
            self.patterns_custom = patterns
            self.definitions = definitions

        return patterns, definitions

    def validate(self, pattern=None):
        """Validate patterns.

        Patterns are validated against example lines in the pattern file.
        The example lines must precede the pattern line and start with '##'.

        Example:
          ##2017-03-02T23:21:07.423 ERROR database exception: FAIL
          ##2017-03-02T23:21:08.553 ERROR exception: generic failure
          EXCEPTION_MESSAGE %{TIMESTAMP} %{LOGLEVEL}( %{WORD:component})? exception:%{DATA:exception}

        """
        self._load_custom()

        err = list()
        for key, value in self.patterns_custom.iteritems():
            if pattern and not re.match(pattern, key):
                continue  # exclude by pattern argument

            logging.info("==> Validating %s (%s) with %s examples", key, value.get('pattern'), len(value['text']))
            if 'text' not in value:
                continue  # no pattern comments
            grok = pygrok.Grok(value['pattern'], custom_patterns=self.definitions)
            for line in value['text']:
                match = grok.match(line)
                if not match:
                    err.append("%s does not match:\n%s" % (key, line))
                else:
                    logging.debug("matched: %s", json.dumps(match, indent=2))

        if err:
            raise Exception("Validation of patterns in %s failed: %s" % self.pattern_file, '\n'.join(err))

        return True

    def show_variables(self, pattern=None):
        """Show all variables used in the pattern."""
        self._load_custom()

        regex_vars = list()
        for key, value in self.patterns_custom.iteritems():
            if pattern and not re.match(pattern, key):
                continue  # exclude by pattern argument

            grok = pygrok.Grok(value['pattern'], custom_patterns=self.definitions)
            regex_pattern = grok.regex_obj.pattern
            regex_vars.extend(re.findall(r'\<([a-z_]*?)\>', regex_pattern))

        logging.info("==> %s", ','.join(sorted(set(regex_vars))))
        return sorted(set(regex_vars))

    def convert_to_pipeline(self):
        """Convert patterns dictionary into Elasticsearch pipeline patterns and definitions

        :returns: (list, dict) -- pipeline patterns, pipeline pattern definitions
        """
        nr_of_patterns = len(self.patterns)
        patterns = deepcopy(self.patterns)

        logging.info("converting %i patterns", nr_of_patterns)
        # filter patterns by the pattern keys
        pipeline_patterns = list()
        for pattern_key in self.pattern_keys:
            for key in patterns.keys():
                if re.findall(pattern_key, key):
                    # add to pipeline
                    pipeline_patterns.append(patterns[key])
                    patterns.pop(key)

        # add all remaining items as pattern definitions
        pattern_definitions = patterns

        logging.info("converted %i patterns to %i pipeline patterns and %i pattern definitions",
                     nr_of_patterns, len(pipeline_patterns), len(pattern_definitions))

        return pipeline_patterns, pattern_definitions
