import os
import imp
import gzip
import logging
from collections import Mapping


def is_gzipped(filepath):
    """Check if a file is gzipped compressed.

    :param filepath: file path

    """
    with open(filepath, 'r') as fd:
        magic = fd.read(2)
    # magic taken from gzip.py
    return magic == '\037\213'


def gunzip(filepath):
    """Gunzip a file

    :param filepath: file path

    """
    logging.info("unzipping %s", filepath)
    with gzip.open(filepath, 'rb') as in_file:
        s = in_file.read()

    unzipped = filepath[:-3]  # remove the '.gz' from the filename
    with open(unzipped, 'w') as out_file:
        out_file.write(s)

    return unzipped


def get_files(directory, suffix=None):
    """Get file paths from directory.

    :param directory: directory path
    :param suffix: file suffix to filter (default: None)

    """
    for name in os.listdir(directory):
        full_path = os.path.join(directory, name)
        if os.path.isdir(full_path):
            for entry in get_files(full_path):
                yield entry
        elif os.path.isfile(full_path) and (not suffix or full_path.endswith(suffix)):
            yield full_path


def define_pattern(filepath, pattern_path):
    """Retrieve pattern name and index name for the given filepath.

    Possible matches for pattern auto-detection:
     - the filename (without the extension)
       example: nginx.log -> nginx
     - the first part of the filename, before an underscore
       example: mongodb_node1.log -> mongodb
     - a part of directory path
       example: /var/log/postgres/node1.log -> postgres

    :param filepath: file path
    :param pattern_path: directory path of pattern files

    """
    logging.info("defining pattern for file %s", filepath)

    paths = filepath.split('/')
    filename, ext = os.path.splitext(os.path.basename(filepath))
    component = filename.split('_')[0]

    patterns = dict()
    for pattern_file in get_files(pattern_path):
        patterns[os.path.basename(pattern_file)] = pattern_file

    pattern = None
    for path in paths:
        if component in patterns:
            pattern = component
            break
        elif filename in patterns:
            pattern = filename
            break
        elif ext:
            while ext:
                filename, ext = os.path.splitext(filename)
                if filename in patterns:
                    pattern = filename
                    break
            if pattern:
                break
        elif path in patterns:
            pattern = path
            break

    if pattern is None:
        raise Exception("no pattern found in %s for %s" % (pattern_path, filepath))

    logging.info("found pattern '%s' for file %s", pattern, filepath)
    return pattern, patterns[pattern]


def load_plugins(plugin_dir):
    """ Load plugins from path """
    def get_plugin_key(filepath, key):
        prefix = os.path.splitext(filepath)[0].replace(plugin_dir, '').replace('/', '.').lstrip('.')
        return '%s.%s' % (prefix, key)

    plugins = dict()
    for filepath in get_files(plugin_dir):
        filename = os.path.basename(filepath)
        if filename.startswith('_') or not filename.endswith('.py'):
            continue

        try:
            plugin_obj = imp.load_source('plugins', filepath)
        except Exception as ex:
            logging.warning("failed to load plugins from %s: %s", filepath, ex)

        for key, val in plugin_obj.__dict__.iteritems():
            if not key.startswith('_') and callable(val):
                combo_key = get_plugin_key(filepath, key)
                if combo_key in plugins:
                    raise Exception("plugin '%s' already set in plugins?" % combo_key)
                plugins[combo_key] = val

    #print plugins
    return plugins


class DotDict(dict):
    """ DotDict base class """
    __getattr__ = dict.get
    __delattr__ = dict.__delitem__
    __setattr__ = dict.__setitem__


class DottedDict(DotDict):
    def update(self, other=None, **kwargs):
        if other is not None:
            for k, v in other.items() if isinstance(other, Mapping) else other:
                if isinstance(self.get(k), dict):
                    self[k].update(v)
                else:
                    self[k] = v
        for k, v in kwargs.items():
            if isinstance(self.get(k), dict):
                self[k].update(v)
            else:
                self[k] = v

class FileInfo(DottedDict):
    def __init__(self, *args, **kwargs):
        self.filepath = None
        self.encoding = None
        self.start_line = None
        self.pattern = None  # TODO - unused
        self.type = None  # TOOD - unused
        self.preprocessor = None

        self.multiline = DottedDict(method=None, pattern=None, include_delimiter=True)

        super(FileInfo, self).__init__(*args, **kwargs)


class Config(DottedDict):
    def __init__(self, *args, **kwargs):
        self.files = list()
        self.pattern = DottedDict(pattern_keys=None,
                                  pattern_path=None,
                                  pattern_include_file=None,
                                  pattern_multiline_method=None)
        self.pipeline = DottedDict(ignore_errors=False,
                                   bulk_size=500,
                                   upload=False,
                                   simulate=False,
                                   host=None,
                                   create=False,
                                   overwrite=False)
        self.index = DottedDict(index_name=None)
        self.general = DottedDict(debug=False,
                                  extra_data=None,
                                  plugin_path=None,
                                  processors_path=None)

        super(Config, self).__init__(*args, **kwargs)
