from __future__ import print_function
from setuptools import setup, find_packages
import io
import codecs
import os
import sys

setup(
    name='dalek',
    version='0.0.2', #dalek.__version__,
    url='http://github.com/wiemdw/dalek/',
    license='Apache Software License',
    author='Wim De Waegeneer',
    install_requires=['elasticsearch>5.0', 'futures==3.0.5', 'pygrok==1.0.0', 'requests>=2.13.0', 'pyaml==16.12.2'],
    author_email='wiemdw@gmail.com',
    description='Elasticsearch 5 helper classes and tools',
    packages=['dalek'],
    include_package_data=True,
    platforms='any',
    classifiers = [
        'Programming Language :: Python',
        'Development Status :: 4 - Beta',
        'Natural Language :: English',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Software Development :: Libraries :: Application Frameworks',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        ],
    scripts = ['dalek_ingest.py']
)
