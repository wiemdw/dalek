-e .

appdirs==1.4.3
astroid==1.4.9
backports.functools-lru-cache==1.4
certifi==2017.4.17
chardet==3.0.3
configparser==3.5.0
elasticsearch==5.4.0
futures==3.0.5
idna==2.5
isort==4.2.5
lazy-object-proxy==1.3.1
mccabe==0.6.1
packaging==16.8
pep8==1.7.0
pkg-resources==0.0.0
pygrok==1.0.0
pyparsing==2.2.0
regex==2017.5.26
requests==2.17.3
six==1.10.0
urllib3==1.21.1
wrapt==1.10.10
pyaml==16.12.2
